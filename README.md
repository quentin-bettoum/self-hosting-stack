# Self Hosting Stack

This is my self-hosting docker stack.

It includes multiple apps :
- [Nginx Proxy Manager](https://nginxproxymanager.com/) *A very simple reverse proxy with built in Let’s Encrypt support and a nice web UI*
- [Adminer](https://www.adminer.org/) *A full-featured database management tool written in PHP to manage all the databases*
- [Portainer](https://www.portainer.io/) *A web app for container management*
- [PiHole](https://pi-hole.net/) *A DNS Sinkhole to block ads and trackers network-wide. I configured the container so it has its own IP on the network to prevent conflicts.*
- [Nextcloud](https://nextcloud.com/) *Productivity platform for shared storage, video calls, and many more features*
- [Wallabag](https://www.wallabag.it) *Save web articles. Read them later. Read them anywhere. Good replacement for Pocket.*
- [FreshRSS](https://www.freshrss.org/) *RSS agregator*
- [WordPress](https://wordpress.com/) *Content Management System*
- [Piwigo](https://piwigo.org/) *Photo gallery software for the web*
- [Searx](https://searx.me/) *The privacy-respecting metasearch engine*
- [Yacy](https://yacy.net/) *A decentralized search engine*
- [Gitea](https://gitea.com/) *Self-hosted git service*
- [ProjectSend](https://www.projectsend.org/) *For file sharing*
- [Mumble](https://en.wikipedia.org/wiki/Mumble_(software)) *FLOSS VoIP server*
- [Netdata](https://www.netdata.cloud/) *A powerful performance and health monitoring software*
- [SMTP Server](https://hub.docker.com/r/ixdotai/smtp) *I use it so NextCloud can send notification emails but it can be linked to other containers*

I used the [docker bridge networks](https://docs.docker.com/network/) to avoid exposing ports for every container. I only open ports when necessary and when I want to access the app from lan.

This stack is used on a Raspberry Pi 4 with 4GB of RAM and [Ubuntu Server 64 bits](https://ubuntu.com/download/raspberry-pi) and so far it's running pretty well. I have an active cooling (a fan case) and the Raspberry Pi rarely goes over 45°C.

Most of the container should work on ARM 32bits but you'd have to change the **mariadb** containers with **biarms/mysql**.
I've built the Yacy, Wallabag, and Searx container myself as I did not find any on docker hub ([here is my account](https://hub.docker.com/u/pieceofcloud)). I built it on a Raspberry Pi 2 (so it is compatible with 32 bits ARM) and I'll try update them (Yacy took between 1 and 2 hours to complete). But I was pretty new to Docker so I did not search anywhere else than docker hub.

To start the stack you'd have to `docker-compose up -d` Nginx first, then Adminer, and then whatever you want. This is because my Nginx compose file creates all the networks to communicate with the other containers, and same for adminer to link with Database containers.
I plan to update this stack with an Ansible playbook to automate the installation and make it easier.